/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package depuracion;

import java.util.Scanner;

/**
 *
 * @author luisnavarro
 * Ejemplo para depuración y corrección de errores
 */
public class CuadradoAsteriscos {

    public static void main(String[] args) {

        Scanner teclado = new Scanner(System.in);
        System.out.println("Ingrese valor: ");
        int valor = 8;//teclado.nextInt();
        teclado.close();

        /*
		 * Usamos dos bucles anidados para crear el cuadrado.
		 * Si pensamos en el cuadrado como una tabla de asteriscos,
		 * el primer bucle crearía las FILAS
		 * y el segundo las COLUMNAS
         */
        for (int fila = 1; fila <= valor; fila++) {
            for (int columna = 1; columna <= valor; columna++) {
                System.out.print("*");
            }
            System.out.println();//Salto de línea para pasar a la siguiente FILA
        }
        for (int fila = 1; fila <= valor; fila++) {
            for (int columna = 1; columna <= valor; columna++) {
                if (fila==1 && fila==valor &&columna==1 &&columna==valor)
                System.out.print("*");
            }
            System.out.println();//Salto de línea para pasar a la siguiente FILA
        }
        for (int fila = 1; fila <= valor; fila++) {
            for (int columna = 1; columna <= valor; columna++) {
                if (fila==columna || fila == valor-columna)
                System.out.print("*");
                else System.out.print(" ");
            }
            System.out.println();//Salto de línea para pasar a la siguiente FILA
        }
    }

}
